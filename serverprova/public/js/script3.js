document.getElementsByTagName("button")[0].addEventListener('click', start);

async function start(){
    console.log('start');
    try{
        let num1 = await getNumber1();
        console.log(num1);
        let num2 = await getNumber2();
        console.log(num2);
        let result = await multi (num1,num2);
        console.log(result);
    }catch(err){
        console.log(err);
    }

}

async function getNumber1(){
    console.log('start getNumber1');
    //let res = await sendToServer('GET','/number1','');
    //console.log(res);
    return await sendToServer('GET','http://localhost:10000/number1','');
}

async function getNumber2(){
    console.log('start getNumber2');
    return await sendToServer('GET','http://localhost:10000/number2','');
}

async function multi(num1, num2){
    console.log('start multi');
    let data = {
        num1: num1,
        num2: num2
    }
    return await sendToServer('POST','http://localhost:10000/multiplicar',data);
}