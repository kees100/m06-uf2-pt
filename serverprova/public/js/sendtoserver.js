module.exports = function sendToServer(method, path, data) {
    return new Promise(function(resolve, reject) {
        // Create a new XMLHttpRequest object
        let xhr = new XMLHttpRequest();

        // Open the XMLHttpRequest with the specified method (GET, POST, etc.) and path
        xhr.open(method, path);

        // Set the content type of the request to be sent as JSON
        xhr.setRequestHeader('Content-Type', 'application/json');

        // Convert the data object to a JSON string
        data = JSON.stringify(data);

        // Send the HTTP request with the data
        xhr.send(data);

        // Define the callback for when the request completes
        xhr.onload = function() {
            // Check if the HTTP status is not 200 (OK)
            if (xhr.status != 200) {
                // Reject the Promise with an Error object containing the HTTP status
                reject(new Error(xhr.status));
            } else {
                // Resolve the Promise with the parsed JSON response
                resolve(JSON.parse(xhr.response));
            }
        };
    });
}

