
let users = [
    {
        "username": "ketsia",
        "email": "ketes15@gmail.com",
        "password": "Maria1515",
        "phone": "699316024",
        "dni": '40451000A'
    },
   
];
const origensDisponibles = ["Barcelona", "Madrid", "Bilbao"];
const destinacionsDisponibles = ["Londres", "Paris", "Roma"];
document.addEventListener("DOMContentLoaded", function(event) {
    document.getElementById("login").style.display="none";
    document.getElementById("registre").style.display="none";
    document.getElementById("reserva").style.display="none";
    setInterval(actualizarData, 1000); 
    actualizarData(); 
    document.getElementById("bregistre").addEventListener('click', function (e) {
        document.getElementById("registre").style.display="inline";
        document.getElementById("login").style.display="none";
        document.getElementById("reserva").style.display="none";

    });
    document.getElementById("blogin").addEventListener('click', function(e) {
        document.getElementById("login").style.display="inline";
        document.getElementById("registre").style.display="none";
        document.getElementById("reserva").style.display="none";
    });
    document.getElementById("e_registre").addEventListener("click", function (e) {
        
        let name = document.getElementById("name").value;
        let email = document.getElementById("email").value;
        let password = document.getElementById("password").value;
        let password2 = document.getElementById("password2").value;
        let phone = document.getElementById("phone").value;
        let dni = document.getElementById("dni").value;
        

        document.querySelectorAll(".error").forEach((element) => (element.innerHTML = ""));

        let errors = false;
        
        //Validació del nom

        if (name.length < 3 || name.length > 10) {
            document.getElementById("nameError").innerHTML = "Nom d'usuari ha de tenir entre 3 i 10 caràcters.";
            errors = true;
        }

        // Validació de format d'email (s'usa una expressió regular simple aquí)
        if (!/^[\w-]+(\.[\w-]+)*@[\w-]+(\.[\w-]+)+$/.test(email)) {
            document.getElementById("emailError").innerHTML = "Format d'email no vàlid.";
            errors = true;
        }

        // Validació de la clau
        if (password.length < 8 || !/[a-z]/.test(password) || !/[A-Z]/.test(password) || !/\d/.test(password)) {
            document.getElementById("passwordError").innerHTML = "La clau ha de tenir almenys 8 caràcters, una majúscula, una minúscula i un número.";
            errors = true;
        }

        // Comparació de les contrasenyes
        if (password !== password2) {
            document.getElementById("password2Error").innerHTML = "Les contrasenyes no coincideixen.";
            errors = true;
        }

        // Validació de telèfon
        if (!/^(93|6)\d{8}$/.test(phone) || phone.length !== 9) {
            document.getElementById("phoneError").innerHTML = "Telèfon no vàlid.";
            errors = true;
        }
        // Validació dni
        const dniPattern = /^[0-9]{8}[A-Z]$/;

        if (!dniPattern.test(dni)) {
            document.getElementById("dniError").innerHTML = "El DNI ha de tenir el format 8 números i una lletra (Exemple: 12345678A).";
            errors = true;
        } else {
            let firstDigit = dni.charAt(0);

  
            if (firstDigit > 4) {
                document.getElementById("dniError").innerHTML = "El primer número del DNI no pot ser major que 4 (0 és vàlid).";
                errors = true;
            } else {
                const letters = 'TRWAGMYFPDXBNJZSQVHLCKE';
                let lastCharacter = dni.charAt(8);
                let numberPart = dni.substring(0, 8);

           
                const remainder = numberPart % 23;
                let validLetter = letters.charAt(remainder);

             
                if (lastCharacter !== validLetter) {
                    document.getElementById("dniError").innerHTML = "La lletra del DNI no correspon al número.";
                    errors = true;
                }
            }
        }


        if (errors) {
            event.preventDefault(); 
        } else {
            afegirUsuari(name, email, password, phone, dni);
        
        }
       
      
    });

    const sendToServer = require('sendToServer');

    document.getElementById("e_login").addEventListener("click", async function (event) {
        event.preventDefault();
        document.getElementById("registre").style.display = "none";
        let loginForm = document.getElementById("login");
        let reservaForm = document.getElementById("reserva");
        let errorMessage = document.getElementById("error-message");
        let username = document.getElementById("l_name").value;
        let password = document.getElementById("l_password").value;
        
        if (!username || !password) {
          errorMessage.textContent = "Si us plau, completa tots els camps.";
        } else {
          errorMessage.textContent = "";
          try {
            // Realizar una solicitud al endpoint '/login' del servidor
            let response = await fetch('/login', {
              headers: {
                "Content-Type": 'application/json',
              },
              method: 'POST',
              body: JSON.stringify({ user: username, password: password })
            });
          
            // Convertir la respuesta a formato JSON
            let data = await response.json();
            return await sendToServer('GET','http://localhost:3000/login','');
          
            // Imprimir los datos en la consola
            console.log(data);
      
            // Realizar acciones adicionales con los datos recibidos, por ejemplo, mostrar otra sección del formulario
            // displayReserva(data);
          } catch (error) {
            console.error(error);
            errorMessage.textContent = "Error en la solicitud al servidor.";
          }
        }
      });
   

    document.getElementById("e_reserva").addEventListener("click", function (event) {
        event.preventDefault(); 

        const origen = document.getElementById("origen").value;
        const desti = document.getElementById("desti").value;
        const dateAnada = new Date(document.getElementById("dateAnada").value);
        const dateTornada = new Date(document.getElementById("dateTornada").value);
        const numPassatgers = parseInt(document.getElementById("num_passatgers").value);

        document.querySelectorAll(".error").forEach((element) => (element.innerHTML = ""));

        let errors = false;

        // Validació de l'origen 
        if (!origensDisponibles.includes(origen)) {
            document.getElementById("origenError").innerHTML = "Origen no válido.";
            errors = true;
        }

        // Validació el destí
        if (!destinacionsDisponibles.includes(desti)) {
            document.getElementById("destiError").innerHTML = "Destino no válido.";
            errors = true;
        }

        // Validació de les dates
        const today = new Date();
        if (dateAnada < today || dateTornada < today || dateAnada > dateTornada) {
            document.getElementById("dataError").innerHTML = "Las fechas no son válidas.";
            errors = true;
        }

        // Validació número de passatgers
        if (numPassatgers < 1 || numPassatgers > 10) {
            document.getElementById("numPassatgersError").innerHTML = "El número de pasajeros debe estar entre 1 y 10.";
            errors = true;
        }

        if (!errors) {
            // Aquí puedes ocultar el formulario de reserva y mostrar el siguiente formulario para los pasajeros.
            document.getElementById("reserva").style.display = "none";
            displayUltimForm(numPassatgers);
        }
    });
});
    function displayReserva() {
        document.getElementById('reserva').style.display = "inline";
        document.getElementById('login').style.display = "block";
    }

   

    function actualizarData() {
        let fechaHoraElemento = document.getElementById('date');
        let fechaHoraActual = new Date();
    
        let diaSemana = fechaHoraActual.toLocaleDateString('es-ES', { weekday: 'long' });
        let dia = fechaHoraActual.getDate();
        let mes = fechaHoraActual.toLocaleDateString('es-ES', { month: 'long' });
        let año = fechaHoraActual.getFullYear();
        let hora = fechaHoraActual.getHours().toString().padStart(2, '0');
        let minutos = fechaHoraActual.getMinutes().toString().padStart(2, '0');
        let segundos = fechaHoraActual.getSeconds().toString().padStart(2, '0');
    
        diaSemana = diaSemana.charAt(0).toUpperCase() + diaSemana.slice(1).toLowerCase();
    
        let fechaHoraFormateada = `${diaSemana}, ${dia} de ${mes} de ${año}, ${hora}:${minutos}:${segundos}`;
    
        fechaHoraElemento.innerHTML = fechaHoraFormateada;
    }
    

function afegirUsuari(name, email, password, phone, dni) {
    
    let nouUsuari = {
        "name": name,
        "email": email,
        "password": password,
        "phone": phone,
        "dni": dni
    };
    users.push(nouUsuari);
    console.log("Nuevo usuario añadido:", nouUsuari);
    document.getElementById("registre").style.display="none";
    document.getElementById("login").style.display="inline";
    

}
function displayUltimForm(numPassatgers) {
    for (let i = 1; i <= numPassatgers; i++) {
        const formValidacio = document.createElement("form");
        formValidacio.innerHTML = `
            <fieldset>
                <legend>Validacio passatgers${i}</legend>
                <label for="passatger${i}_nom">Nom del passatger ${i}</label>
                <input type="text" id="passatger${i}_nom" name="passatger${i}_nom" required><br>
                <label for="passatger${i}_dni">DNI del passatger ${i}</label>
                <input type="text" id="passatger${i}_dni" name="passatger${i}_dni" required><br>
                <div id="resultat"></div>
                <button class="button" id="e_passatger${i}">Enviar</button>
                
            </fieldset>
        `;

        // Agregar el formulario de validación al documento
        document.body.appendChild(formValidacio);

        // Escuchar el evento de envío del formulario de validación para cada pasajero
        formValidacio.querySelector(`#e_passatger${i}`).addEventListener("click", function (e) {
            e.preventDefault(); // Evita que se envíe el formulario

            let nomPassatger = document.getElementById(`passatger${i}_nom`).value;
            let dniPassatger = document.getElementById(`passatger${i}_dni`).value;

            // Validar si el pasajero está en el array de usuarios
            let usuari = users.find(user => user.name === nomPassatger && user.dni === dniPassatger);

            if (usuari) {
                let resultatReserva = document.getElementById("resultat");
                resultatReserva.innerHTML += `S'ha reservat el vol per al passatger ${i}.<br>`;
            } else {
               let resultatReserva = document.getElementById("resultat");
                resultatReserva.innerHTML += `Les dades del passatger ${i} no son valides.<br>`;
            }
        });
    }
}

// document.addEventListener("DOMContentLoaded", () => {

    //     document.querySelector('#btn1').addEventListener('click', guardar);
    //     document.querySelector('#btn2').addEventListener('click', mostrarInfo);
    //     document.querySelector('#btn3').addEventListener('click', crearCookie);
    
    
    
    
    // });
    
    // function guardar() {
    //     console.log('funcionar guardar')
    //     let info = document.querySelector('input')[0].value;
    //     console.log(info);
    //     localStorage.setItem('InputInfo', info);
    //     let user = {
    //         name: "Ket",
    //         age: 35
    //     }
    //     localStorage.setItem('user', JSON.stringify(user));   //Solo se pueden guardar strings
    // }
    
    // function mostrarInfo() {
    //     console.log('funcionar mostrar');
    //     let info = localStorage.getItem('InputInfo');
    //     console.log(info)
    //     console.log(localStorage.inputInfo);
    //     delete localStorage.inputInfo;
    //     console.log(localStorage.inputInfo);
    //     console.log(localStorage.obj);
    //     console.log(JSON.parse(localStorage.obj));
      
    // }
    
    // function crearCookie() {
    //     document.cookie = "user=ketsia";
    
    // }
    // function eliminarCookie() {
        
    // }


