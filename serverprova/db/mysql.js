module.exports = {
    query: function (sql) {
      return new Promise((resolve, reject) => {
        // Import the 'mysql' library
        let mysql = require('mysql');
  
        // Create a MySQL connection
        let connection = mysql.createConnection({
          host: 'localhost',
          user: 'root',
          password: '',
          database: 'm06'
        });
  
        // Establish a connection to the MySQL server
        connection.connect();
  
        // Perform a query on the database
        connection.query(sql, function (err, rows, fields) {
          // Handle errors, if any
          if (err) reject(err);
  
          // If successful, resolve the promise with the query result
          resolve(rows);
        });
  
        // Close the MySQL connection
        connection.end();
      });
    }
  };
